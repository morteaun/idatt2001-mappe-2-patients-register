package edu.ntnu.idatt2001.morteaun;

import java.util.Objects;

/**
 * Represents a patient, has all getters and setters
 * as well as a method for getting the patient as a
 * string using the delimiter set in DAO.
 *
 * @author morten
 */
public class Patient {

    private String socialSecurityNumber;
    private String firstName;
    private String lastName;
    private String diagnosis = "";
    private String generalPractitioner;

    /**
     * Constructor containing all patient properties except diagnosis
     * @param fName First name to be set
     * @param lName Last name to be set
     * @param generalPractitioner The full name of the general practitioner
     * @param socialSecurityNumber The social security number to be set
     * @throws IllegalArgumentException If SSC(social security number/code) contains anything other than digits. Has an
     *                                  exception for the string "socialSecurityNumber".
     */
    public Patient(String fName, String lName, String generalPractitioner, String socialSecurityNumber) throws IllegalArgumentException{
        if (socialSecurityNumber.chars().allMatch(Character::isDigit) || socialSecurityNumber.equalsIgnoreCase("socialsecuritynumber")) {
            this.firstName = Objects.requireNonNull(fName, "Can't be null");
            this.lastName = Objects.requireNonNull(lName, "Can't be null");
            this.socialSecurityNumber = Objects.requireNonNull(socialSecurityNumber, "Can't be null");
            this.generalPractitioner = generalPractitioner;
        }
        else {throw new IllegalArgumentException("Social security number must consist of only digits");}
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public String getGeneralPractitioner() {
        return generalPractitioner;
    }

    /**
     * Sets the stored socialSecurityNumber as the entered parameter.
     * Checks if parameter is null and if parameter only contains digits.
     * If parameter contains something other than digits, an
     * Exception of type IllegalArgumentException is thrown.
     *
     * @param socialSecurityNumber The new socialSecurityNumber
     * @throws IllegalArgumentException If socialSecurityNumber contains something other than digits
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) throws IllegalArgumentException{
        if (socialSecurityNumber.chars().allMatch(Character::isDigit)) {
            this.socialSecurityNumber = Objects.requireNonNull(socialSecurityNumber, "Can't be null");
        }
        else {
            throw new IllegalArgumentException("Social security number must consist of only digits");
        }
    }

    public void setFirstname(String firstName) {
        this.firstName = Objects.requireNonNull(firstName, "Can't be null");
    }

    public void setLastName(String lastName) {
        this.lastName = Objects.requireNonNull(lastName, "Can't be null");
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner = generalPractitioner;
    }

    /**
     * Convert all the patient information into one string, separated by a semicolon.
     * This semicolon should be the same as the delimiter set in DAO
     *
     * @return A string with elements separated by a semicolon
     */
    public String getAsCSV(){
        return firstName + ";" + lastName + ";" + generalPractitioner + ";" + socialSecurityNumber + ";" + diagnosis;
    }
}
