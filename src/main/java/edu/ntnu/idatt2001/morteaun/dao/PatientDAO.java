package edu.ntnu.idatt2001.morteaun.dao;

import edu.ntnu.idatt2001.morteaun.PatientRegister;
import edu.ntnu.idatt2001.morteaun.Patient;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * The data access object that provides access to this applications' persistence storage
 *
 * @author morten
 */
public class PatientDAO {
    public final String DELIMITER = ";";

    public PatientDAO() {
    }

    /**
     * Uses FileInputStream to read patients from CSV file and adds them to the register.
     * Uses try to automatically close the connection if it fails.
     * Prints out the stack trace if IOException is thrown.
     *
     * @param patientRegister Register to add patients to
     */
    public void fillRegisterFromCSV(PatientRegister patientRegister) {
        String line;
        try (FileInputStream fileInputStream = new FileInputStream(patientRegister.getLoadFrom());
             InputStreamReader isr = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
             BufferedReader br = new BufferedReader(isr)) {
            while ((line = br.readLine()) != null) {
                String[] patient = line.split(DELIMITER);
                if (patient.length == 4 && patient[2] != null) {
                    patientRegister.addPatient(patient[0], patient[1], patient[2], patient[3]);
                }
                else if (patient.length == 5){
                    patientRegister.addPatientWithDiagnosis(patient[0], patient[1], patient[2], patient[3], patient[4]);
                }
                else {
                    patientRegister.addPatient(patient[0], patient[1], "", patient[3]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Uses OutputStream and getAsCSV to save patients to CSV file.
     * Uses try to automatically close the connection if it fails.
     * Prints out the stack trace if IOException is thrown.
     * @param patientRegister register to be saved
     */
    public void saveToCSV(PatientRegister patientRegister) {
        try (OutputStream os = new FileOutputStream(patientRegister.getSaveFile());
             PrintWriter pw = new PrintWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8))) {
            patientRegister.getPatientList().stream().map(Patient::getAsCSV).forEach(pw::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
