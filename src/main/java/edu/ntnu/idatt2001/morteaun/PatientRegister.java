package edu.ntnu.idatt2001.morteaun;

import edu.ntnu.idatt2001.morteaun.dao.PatientDAO;

import java.io.File;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Manages all the patients in an ArrayList. Has methods to work with this ArrayList.
 * Also contains methods to help save and load the ArrayList.
 *
 * @author morten
 */
public class PatientRegister {
    private final ArrayList<Patient> patientList;
    private final PatientDAO DAO;
    private File saveFile;
    private File loadFrom;

    /**
     * Creates a new ArrayList to hold patients as well as a DAO to help save and load.
     * saveFile and loadFrom contains the set default location that is looked at when
     * wanting to save and load.
     */
    public PatientRegister() {
        patientList = new ArrayList<>();
        DAO = new PatientDAO();
        saveFile = new File("src/main/resources/csv/Patients.csv");
        loadFrom = new File("src/main/resources/csv/Patients.csv");
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }

    /**
     * Finds the patient the parameter belongs to
     *
     * @param sSC The social security number/code of the person you are looking for
     * @return The patient object that owns the ssc equal to the parameter
     */
    public Patient getPatientFromSSC(String sSC){
        return patientList.stream().filter(e -> e.getSocialSecurityNumber().equals(sSC)).collect(Collectors.toList()).get(0);
    }

    /**
     * Adds a patient containing the entered parameter to the ArrayList.
     * If the entered social security number already exists in the ArrayList, an IllegalArgumentException is thrown
     *
     * @param firstName First name to be set
     * @param lastName Last name to be set
     * @param generalPractitioner The full name of the general practitioner
     * @param socialSecurityNumber The desired social security number
     * @throws IllegalArgumentException If the social security number already exists within the register
     */
    public void addPatient(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber) throws IllegalArgumentException{
        if (sSCExists(socialSecurityNumber)){
            throw new IllegalArgumentException("That social security number is already in use");
        } else {
            patientList.add(new Patient(firstName, lastName, generalPractitioner, socialSecurityNumber));
        }
    }

    /**
     * Adds a patient containing all elements including a diagnosis
     * If the entered social security number already exists in the ArrayList, an IllegalArgumentException is thrown
     *
     * @param firstName First name to be set
     * @param lastName Last name to be set
     * @param generalPractitioner The full name of the general practitioner
     * @param socialSecurityNumber The desired social security number
     * @param diagnosis The diagnosis to be set
     * @throws IllegalArgumentException If the social security number already exists within the register
     */
    public void addPatientWithDiagnosis(String firstName, String lastName, String generalPractitioner, String socialSecurityNumber, String diagnosis) throws  IllegalArgumentException{
        if (sSCExists(socialSecurityNumber)){
            throw new IllegalArgumentException("That social security number is already in use");
        } else {
            Patient patient = new Patient(firstName, lastName, generalPractitioner, socialSecurityNumber);
            patient.setDiagnosis(diagnosis);
            patientList.add(patient);
        }
    }

    /**
     * Removes the patient the entered social security number belongs to
     *
     * @param socialSecurityNumber The social security number of the patient to be removed
     * @throws IllegalArgumentException If the entered social security number doesn't belong to any patient in the register
     */
    public void removePatient(String socialSecurityNumber) throws IllegalArgumentException{
        if (!sSCExists(socialSecurityNumber)){
            throw new IllegalArgumentException("No Patient with that social security number could be found");
        } else {
            patientList.removeIf(patient -> patient.getSocialSecurityNumber().equals(socialSecurityNumber));
        }
    }

    public File getSaveFile() {
        return saveFile;
    }

    public File getLoadFrom() {
        return loadFrom;
    }


    /**
     * Changes the default location that is looked towards when wanting to save register
     *
     * @param saveFile The new file that register should be saved to
     */
    public void setSaveFile(File saveFile) {
        this.saveFile = saveFile;
    }

    /**
     * Changes the default location that is looked towards when wanting to load a register from a file
     *
     * @param loadFrom The new location that register should be loaded from
     */
    public void setLoadFrom(File loadFrom) {
        this.loadFrom = loadFrom;
    }

    /**
     * Clears the current register and fills it with the data that is in the given file
     *
     * @param file The file the new data should be loaded from
     */
    public void loadTable(File file) {
        if (file == null) {
            throw new NullPointerException();
        } else {
            patientList.clear();
            setLoadFrom(file);
            DAO.fillRegisterFromCSV(this);
        }
    }

    /**
     * Runs the method in the DAO responsible for saving the registry.
     */
    public void saveTable(){
        DAO.saveToCSV(this);
    }

    /**
     * Runs through the registry to check if the entered social security number already exists
     *
     * @param sSC The social security number to be checked if exists
     * @return {@code true} If the same number already exists
     *         {@code false} If not
     */
    public boolean sSCExists(String sSC){
        for (Patient patient : patientList) {
            if (sSC.trim().equals(patient.getSocialSecurityNumber())) {
                return true;
            }
        }
        return false;
    }

}
