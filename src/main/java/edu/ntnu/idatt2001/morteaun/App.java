package edu.ntnu.idatt2001.morteaun;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * This class starts the application
 *
 * @author morten
 * @version 1.0
 */
public class App extends Application {
    private static Scene scene;
    private static PatientRegister patientRegister;

    /**
     * Loads in the fxml file from a given string of the name.
     *
     * @param fxml the name of the fxml file to find and return.
     * @return the fxml file loaded.
     * @throws IOException if the file isn't found.
     */
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }

    public static PatientRegister getPatientRegister() {
        return patientRegister;
    }

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("primary"), 601, 695);
        stage.setScene(scene);
        stage.getIcons().add(new Image("file:/favicon.png"));
        stage.setMinWidth(615);
        stage.setMinHeight(435);
        stage.show();
    }

    @Override
    public void init() throws Exception {
        super.init();
        patientRegister = new PatientRegister();
    }
}
