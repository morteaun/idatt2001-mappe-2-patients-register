package edu.ntnu.idatt2001.morteaun.controllers;

import edu.ntnu.idatt2001.morteaun.PatientRegister;
import edu.ntnu.idatt2001.morteaun.Patient;
import edu.ntnu.idatt2001.morteaun.App;
import edu.ntnu.idatt2001.morteaun.factory.GUIFactory;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * Controller class for the main window of the application
 *
 * @author morten
 */
public class PrimaryController implements Initializable {

    public TableView<Patient> patientTable;
    public PatientRegister patientRegister;
    public TableColumn firstName;
    public TableColumn lastName;
    public TableColumn socialSecurityNumber;

    /**
     * Called to initialize the controller after the root element has been completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resources The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        socialSecurityNumber.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        this.patientRegister = App.getPatientRegister();
        patientTable.setRowFactory( t -> {
            TableRow<Patient> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())){
                    editPatient();
                } else {row.requestFocus();}
            });
            return row;
        });
    }

    /**
     * Finds and returns the social security number of the patient that is currently selected in the TableView
     *
     * @return SSC of the selected person
     */
    private String getSelectedPatientSSC(){
        return patientTable.getSelectionModel().getSelectedItem().getSocialSecurityNumber();
    }

    /**
     * Opens the add patient window where you can enter patient information.
     * Locks the program so you must close or finish the new window before being able to do more in the primary view
     */
    public void addPatient(){
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/addPatient.fxml"));
            int listLength = patientRegister.getPatientList().size();
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();

            if (patientRegister.getPatientList().size() > listLength) {
                patientTable.getItems().add(patientRegister.getPatientList().get(patientRegister.getPatientList().size() - 1));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Opens the edit patient window where the saved patient information is already displayed
     * Locks the program so you must close or finish the new window before being able to do more in the primary view
     * Shows an alert if no patient is selected stating that one must be selected to enter the edit view
     */
    public void editPatient(){
        try {
            String selectedSSC = getSelectedPatientSSC();
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/editPatient.fxml"));
            Parent root = fxmlLoader.load();

            EditPatientController editPatientController = fxmlLoader.getController();

            editPatientController.initializeEdit(selectedSSC);

            Stage stage = new Stage();
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();
            refreshPatientTable();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Patient is not selected. Please select which patient you want to edit.");
            alert.setTitle("Select patient to edit");

            alert.showAndWait();
        }
    }

    /**
     * Opens a warning box asking if the selected patient really is to be removed. Removes the patient if OK is pressed,
     * closes the alert if CANCEL is pressed.
     * Shows an alert if no patient is selected stating that the patient must be selected for the user to delete it
     */
    public void removePatient(){
        try {
            Alert alert = GUIFactory.getAlert("warning");
            alert.setTitle("Delete");
            alert.setHeaderText("Are you sure you want to delete patient with \nsocial security number: " + getSelectedPatientSSC() + "?");
            alert.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);
            Optional<ButtonType> showAlert = alert.showAndWait();
            ButtonType result = showAlert.orElse(ButtonType.CANCEL);
            if (result == ButtonType.OK) {
                patientRegister.removePatient(getSelectedPatientSSC());
                refreshPatientTable();
            }
        } catch (Exception e) {
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("No patient is selected. Please select which patient you want to delete.");
            alert.setTitle("Select patient to delete");

            alert.showAndWait();
        }
    }

    /**
     * Refreshes the TableView to display the up-to-date information stored in the register
     */
    public void refreshPatientTable(){
        patientTable.getItems().setAll(FXCollections.observableArrayList(patientRegister.getPatientList()));
    }

    /**
     * Imports all the data from a chosen csv file. Using the chooseCSV method to pick which file to import from
     */
    public void importCSV(){
        try {
            FileChooser chosenFile = chooseCSV();
            File file = chosenFile.showOpenDialog(patientTable.getParent().getScene().getWindow());
            patientRegister.loadTable(file);
            refreshPatientTable();
            System.out.println("CSV imported!");
        } catch (NullPointerException e){
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Selected file did not contain any useable data \nPlease select another file or cancel the operation");
            alert.setTitle("Selected file did not work");

            alert.showAndWait();
        }
    }

    /**
     * Exports all the data to a chosen csv file. Using the chooseCSV method one can either create a brand new
     * file, or one can overwrite an already existing csv file.
     */
    public void exportCSV(){
        FileChooser chosenFile = chooseCSV();
        try {
            File file = chosenFile.showSaveDialog(patientTable.getParent().getScene().getWindow());
            patientRegister.setSaveFile(file);
            patientRegister.saveTable();
            System.out.println("CSV exported!");
        } catch (RuntimeException ignored){}
    }

    /**
     * Set up a FileChooser for selecting files. Added filter so that only csv files can be selected
     * @return an instance of FileChooser with the added filter
     */
    public FileChooser chooseCSV(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("src/main/resources/csv"));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files", "*.csv"));
        return fileChooser;
    }

    /**
     * A method for closing the application
     */
    public void exit(){
        Stage stage = (Stage) patientTable.getScene().getWindow();
        stage.close();
    }

    /**
     * Displays an about window with the basic application version and some filler text
     * @throws IOException If it is unable to load AppVersion.properties, which should hold the version information
     */
    public void about() throws IOException {
        final Properties properties = new Properties();
        properties.load(this.getClass(). getClassLoader().getResourceAsStream("AppVersion.properties"));
        Alert alert = GUIFactory.getAlert("information");
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patients Register \n" + properties.getProperty("version"));
        alert.setContentText("It appears you have come seeking answers... \nToo bad there is no information to be found here");

        alert.showAndWait();
    }
}
