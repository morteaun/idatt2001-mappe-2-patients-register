package edu.ntnu.idatt2001.morteaun.controllers;

import edu.ntnu.idatt2001.morteaun.App;
import edu.ntnu.idatt2001.morteaun.PatientRegister;
import edu.ntnu.idatt2001.morteaun.factory.GUIFactory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Window;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controller class for the add patient window
 * @author morten
 */
public class AddPatientController implements Initializable {

    @FXML
    public Button cancelButton;
    @FXML
    public Button okButton;
    @FXML
    TextField firstName;
    @FXML
    TextField lastName;
    @FXML
    TextField sSC;
    private PatientRegister register;

    /**
     * Run after its root element has been completely processed.
     *
     * @param url The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resourceBundle The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        register = App.getPatientRegister();
        okButton.setOnAction(actionEvent -> addPatient(register));
        cancelButton.setOnAction(actionEvent -> close());
        firstName.setOnKeyTyped(e -> {
            if (firstName.getText().length() > 30) {
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("First name too long!");
                maxCharactersReached.setContentText("First name can only be 30 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        lastName.setOnKeyTyped(e -> {
            if (lastName.getText().length() > 30) {
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Last name too long!");
                maxCharactersReached.setContentText("Last name can only be 30 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        sSC.setOnKeyTyped(e -> {
            if (sSC.getText().length() > 11) {
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Social security number too long!");
                maxCharactersReached.setContentText("Social security number should only consist of 11 numbers");
                maxCharactersReached.showAndWait();
            }
            else if (!sSC.getText().matches("[0-9]+")){
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Illegal input");
                maxCharactersReached.setHeaderText("Wrong input!");
                maxCharactersReached.setContentText("Social security number should only consist of numbers");
                maxCharactersReached.showAndWait();
            }
        });
    }

    /**
     * Adds a patient with the given first name, last name and social security number, alongside a blank general practitioner.
     * @param fName The first name of the patient
     * @param lName The last name of the patient
     * @param sSC The social security number of the patient
     */
    private void registerPatient(String fName, String lName, String sSC){
        register.addPatient(fName, lName, "", sSC);
    }

    /**
     * Adds a patient with the inputted information to the register
     * Alerts ar shown if: The entered social security number already exists in the register,
     *                     Either first name, last name or social security number is missing
     *                  or the entered social security number contains other characters than numbers
     * If none of the cases over apply, the patient is added through the registerPatient method.
     * @param register The active PatientRegister
     */
    private void addPatient(PatientRegister register){
        if (register == null){registerPatient(firstName.getText(), lastName.getText(), sSC.getText());}
        else if (register.sSCExists(sSC.getText())){
            System.out.println("Social security number already exists");
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Social security number already exists in register. \nPlease delete existing patient or choose new number");
            alert.setTitle("Social security number already exists");

            alert.showAndWait();
        }
        else if (firstName.getText().isEmpty() || lastName.getText().isEmpty() || sSC.getText().isEmpty()){
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Either first, last name or social security number is missing");
            alert.setTitle("Elements missing");

            alert.showAndWait();
        }
        else if (!sSC.getText().matches("[0-9]+")){
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Social security number should consist of only numbers");
            alert.setTitle("Social security number wrong format");

            alert.showAndWait();
        }
        else {
            registerPatient(firstName.getText(), lastName.getText(), sSC.getText());
            close();
        }
    }

    /**
     * Closes the add patient window
     */
    private void close(){
        Window window = okButton.getScene().getWindow();
        window.hide();
    }
}
