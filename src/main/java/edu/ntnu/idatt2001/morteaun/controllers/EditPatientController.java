package edu.ntnu.idatt2001.morteaun.controllers;

import edu.ntnu.idatt2001.morteaun.App;
import edu.ntnu.idatt2001.morteaun.Patient;
import edu.ntnu.idatt2001.morteaun.PatientRegister;
import edu.ntnu.idatt2001.morteaun.factory.GUIFactory;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Window;

/**
 * Controller class for the edit patient window
 * @author morten
 */
public class EditPatientController {

    @FXML
    public Button cancelButton;
    @FXML
    public Button okButton;
    @FXML
    public TextField generalPractitioner;
    @FXML
    public TextField diagnosis;
    @FXML
    TextField firstName;
    @FXML
    TextField lastName;
    @FXML
    TextField sSC;
    private Patient selectedPatient;
    private PatientRegister register;

    /**
     * Initializes the edit task view with the information belonging to the
     * patient the given social security number belongs to
     *
     * @param socialSecurityNumber Social security number for the desired patient
     */
    public void initializeEdit(String socialSecurityNumber) {
        selectedPatient = App.getPatientRegister().getPatientFromSSC(socialSecurityNumber);
        register = App.getPatientRegister();
        okButton.setOnAction(actionEvent -> setPatient(register, selectedPatient, socialSecurityNumber));
        cancelButton.setOnAction(actionEvent -> close());
        firstName.setText(selectedPatient.getFirstName());
        diagnosis.setText(selectedPatient.getDiagnosis());
        firstName.setOnKeyTyped(e -> {
            if (firstName.getText().length() > 30) {
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("First name too long!");
                maxCharactersReached.setContentText("First name can only be 30 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        lastName.setText(selectedPatient.getLastName());
        lastName.setOnKeyTyped(e -> {
            if (lastName.getText().length() > 30) {
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Last name too long!");
                maxCharactersReached.setContentText("Last name can only be 30 characters long");
                maxCharactersReached.showAndWait();
            }
        });
        sSC.setText(selectedPatient.getSocialSecurityNumber());
        sSC.setOnKeyTyped(e -> {
            if (sSC.getText().length() > 11) {
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Max limit");
                maxCharactersReached.setHeaderText("Social security number too long!");
                maxCharactersReached.setContentText("Social security number should only consist of 11 numbers");
                maxCharactersReached.showAndWait();
            }
            else if (!sSC.getText().matches("[0-9]+")){
                Alert maxCharactersReached = GUIFactory.getAlert("warning");
                maxCharactersReached.setTitle("Illegal input");
                maxCharactersReached.setHeaderText("Wrong input!");
                maxCharactersReached.setContentText("Social security number should only consist of numbers");
                maxCharactersReached.showAndWait();
            }
        });
        generalPractitioner.setText(selectedPatient.getGeneralPractitioner());
    }

    /**
     * Changes the information of the selected patient to the new values.
     * Alerts ar shown if: The new social security number already exists in the register,
     *                     Either first name, last name or social security number is missing,
     *                     Social security number contains other characters than numbers
     *                  or Diagnosis is trying to be added without a general practitioner also being present
     * If none of the cases over apply the entered values are applied via Patient setters
     * @param register The active PatientRegister
     * @param selectedPatient Tha patient that is to be edited
     * @param oldSSC The patients social security number before entering the edit view
     */
    private void setPatient(PatientRegister register, Patient selectedPatient, String oldSSC){
        if (register.sSCExists(sSC.getText()) && !sSC.getText().equals(oldSSC)){
            System.out.println("Social security number already exists");
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Social security number already exists in register. \nPlease delete existing patient or choose new number");
            alert.setTitle("Social security number already exists");

            alert.showAndWait();
        }
        else if (firstName.getText().isEmpty() || lastName.getText().isEmpty() || sSC.getText().isEmpty()){
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Either first, last name or social security number is missing");
            alert.setTitle("Elements missing");

            alert.showAndWait();
        }
        else if (!sSC.getText().matches("[0-9]+")){
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("Social security number should consist of only numbers");
            alert.setTitle("Social security number wrong format");

            alert.showAndWait();
        }
        else if (!diagnosis.getText().isEmpty() && generalPractitioner.getText().strip().isEmpty()) {
            Alert alert = GUIFactory.getAlert("information");
            alert.setContentText("A general practitioner is required to enter a diagnosis");
            alert.setTitle("Social security number wrong format");

            alert.showAndWait();
        }
        else {
            selectedPatient.setFirstname(firstName.getText());
            selectedPatient.setLastName(lastName.getText());
            selectedPatient.setSocialSecurityNumber(sSC.getText());
            selectedPatient.setGeneralPractitioner(generalPractitioner.getText());
            selectedPatient.setDiagnosis(diagnosis.getText());
            close();
        }
    }

    /**
     * Closes the edit window
     */
    private void close(){
        Window window = okButton.getScene().getWindow();
        window.hide();
    }
}
