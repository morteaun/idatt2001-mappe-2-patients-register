package edu.ntnu.idatt2001.morteaun.factory;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * A factory class that has the functionality to create all GUI-elements needed for the application.
 * However as of version 1.0, only the Alert generation is being used due fxml handling the other elements.
 *
 * @author morten
 */
public class GUIFactory {

    /**
     * Creates the a Node of the desired type
     * As of version 1.0, the supported nodes are:
     *                    MenuBar, Toolbar, Vbox, HBox and TableView
     * @param nodeType A string stating which node type that is wanted
     * @return The newly created node of the type that was requested
     */
    public static Node getNode(String nodeType){
        if (nodeType == null){
            return null;
        }
        if (nodeType.equalsIgnoreCase("menuBar")){
            MenuBar menuBar= new MenuBar();
            Menu file = new Menu("file");
            Menu edit = new Menu("edit");
            Menu help = new Menu("help");
            menuBar.getMenus().addAll(file, edit, help);

            return menuBar;
        } else if (nodeType.equalsIgnoreCase("toolBar")){
            ToolBar toolBar = new ToolBar();
            Button saveBtn = new Button("Save");
            Button editBtn = new Button("Edit");
            Button printBtn = new Button("Print");
            toolBar.getItems().addAll(saveBtn, editBtn, printBtn);

            return toolBar;
        } else if (nodeType.equalsIgnoreCase("vBox")){
            VBox vBox = new VBox();
            vBox.setFillWidth(true);

            return vBox;
        } else if (nodeType.equalsIgnoreCase("hBox")){
            HBox hBox = new HBox();
            hBox.setFillHeight(true);

            return hBox;
        } else if (nodeType.equalsIgnoreCase("tableView")){
            return new TableView<>();
        }
        return null;
    }

    /**
     * Creates the an Alert of the desired type
     * As of version 1.0, the supported alerts are:
     *                    WARNING and INFORMATION
     * @param alertType A string stating which alert type that is wanted
     * @return The newly created Alert of the type that was requested
     */
    public static Alert getAlert(String alertType){
        if (alertType == null){
            return null;
        } else if (alertType.equalsIgnoreCase("warning")){
            return new Alert(Alert.AlertType.WARNING);
        } else if (alertType.equalsIgnoreCase("information")){
            return new Alert(Alert.AlertType.INFORMATION);
        }
        return null;
    }
}
