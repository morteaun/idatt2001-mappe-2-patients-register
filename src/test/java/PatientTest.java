import edu.ntnu.idatt2001.morteaun.Patient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PatientTest {
    Patient testPatient;

    @BeforeEach
    public void reset() {
        testPatient = new Patient("Test", "Testssen", "gp", "1234");
    }

    @Nested
    public class setterTests {
        @Test
        public void setFnameTest() {
            testPatient.setFirstname("Test1");
            assertEquals("Test1", testPatient.getFirstName());
        }

        @Test
        public void setLnameTest() {
            testPatient.setLastName("Testssen1");
            assertEquals("Testssen1", testPatient.getLastName());
        }

        @Test
        public void setSocialSecurityNumberTest() {
            testPatient.setSocialSecurityNumber("123");
            assertEquals("123", testPatient.getSocialSecurityNumber());
        }

        @Test
        public void setGeneralPractitionerTest() {
            testPatient.setGeneralPractitioner("gp1");
            assertEquals("gp1", testPatient.getGeneralPractitioner());
        }

        @Test
        public void setDiagnosisTest() {
            testPatient.setDiagnosis("diagnosis");
            assertEquals("diagnosis", testPatient.getDiagnosis());
        }
    }

    @Test
    public void getAsCSVTest() {
        assertEquals(testPatient.getAsCSV(), "Test;Testssen;gp;1234;");
    }
}
