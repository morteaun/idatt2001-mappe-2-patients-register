import edu.ntnu.idatt2001.morteaun.Patient;
import edu.ntnu.idatt2001.morteaun.PatientRegister;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

public class PatientRegisterTest {
    private final PatientRegister register = new PatientRegister();

    @BeforeEach
    public void reset(){
        register.loadTable(new File("src/test/resources/TestData.csv"));
    }

    @Nested
    class AddPatientTests {
        @Test
        public void addPatientTest() {
            int originalSize = register.getPatientList().size();
            register.addPatient("test", "testssen", "gp", "123");
            assertEquals(register.getPatientList().size(), originalSize + 1);
        }

        @Test
        public void addPatientWithIllegalSSCTest() {
            Exception exception = assertThrows(IllegalArgumentException.class, () -> {
                register.addPatient("test", "testssen", "gp", "dette er ikke en gyldig ssc");
            });
            String expectedMessage = "Social security number must consist of only digits";
            String actualMessage = exception.getMessage();
            assertTrue(actualMessage.contains(expectedMessage));
        }

        @Test
        public void addDuplicatedSSCTest() {
            Exception exception = assertThrows(IllegalArgumentException.class, () -> {
                register.addPatient("test", "testssen", "", "29104300764");
            });
            String expectedMessage = "That social security number is already in use";
            String actualMessage = exception.getMessage();
            assertTrue(actualMessage.contains(expectedMessage));
        }
    }

    @Nested
    class GetPatientBySSCTests {
        @Test
        public void getPatientBySSCTest() {
            assertEquals(register.getPatientFromSSC("29104300764"), register.getPatientList().get(0));
        }

        @Test
        public void getPatientByNonexistentSSCTest() {
            Exception exception = assertThrows(IndexOutOfBoundsException.class, () -> {
                register.getPatientFromSSC("29");
            });
            String expectedMessage = "Index 0 out of bounds for length 0";
            String actualMessage = exception.getMessage();
            assertTrue(actualMessage.contains(expectedMessage));
        }
    }

    @Nested
    class RemovePatientTests {
        @Test
        public void removePatientTest() {
            register.removePatient("29104300764");
            assertFalse(register.sSCExists("29104300764"));
        }

        @Test
        public void removeNonexistentPatientTest() {
            Exception exception = assertThrows(IllegalArgumentException.class, () -> {
                register.removePatient("29");
            });
            String expectedMessage = "No Patient with that social security number could be found";
            String actualMessage = exception.getMessage();
            assertTrue(actualMessage.contains(expectedMessage));
        }
    }

    @Test
    public void saveTest(){
        PatientRegister oldRegister = new PatientRegister();
        oldRegister.loadTable(new File("src/test/resources/TestData.csv"));
        PatientRegister saveTestRegister = new PatientRegister();

        saveTestRegister.addPatient("test", "testssen", "gp", "1234");
        File saveFile = new File("src/test/resources/TestData.csv");
        saveTestRegister.setSaveFile(saveFile);
        saveTestRegister.saveTable();

        reset();
        assertEquals("1234", register.getPatientList().get(0).getSocialSecurityNumber());

        oldRegister.setSaveFile(saveFile);
        oldRegister.saveTable();
    }
}
